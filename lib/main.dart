import 'package:flutter/material.dart';
import 'package:flutter_trip/network/NetWorkTestFirst.dart';

import 'navigator/tab_navigator.dart';
import 'network/NetWorkTestSecond.dart';

void main() {
 // runApp(const MyApp());
 // runApp(const NetWorkTest());
  runApp(const NetWorkTestSecond());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

   @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home:  TabNavigator(),
    );
  }
}


