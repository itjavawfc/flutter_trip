import 'package:flutter/material.dart';
import 'dart:convert' ;

import 'package:http/http.dart' as http;

class NetWorkTestSecond extends StatefulWidget {
  const NetWorkTestSecond({super.key});

  @override
  State<NetWorkTestSecond> createState() => _NetWorkTestSecondState();
}

class _NetWorkTestSecondState extends State<NetWorkTestSecond> {
  String showResult='';

  Future<CommonModel> fetchPost() async{
    var url =
    Uri.https('www.geekailab.com', '/io/flutter_app/json/test_common_model.json', {'q': '{http}'});
    var response = await http.get(url);
    Utf8Decoder utf8decoder=Utf8Decoder();

    final result=json.decode(utf8decoder.convert(response.bodyBytes));
    return  CommonModel.fromJson(result);

  }

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      home: Scaffold(
        appBar:AppBar(
          title: Text('Http'),
        ),
        body:  FutureBuilder<CommonModel>(
          future: fetchPost(),
          builder: (BuildContext context , AsyncSnapshot<CommonModel> snapshot){
            
            switch(snapshot.connectionState){
              case ConnectionState.waiting:
                return Center(
                  child: CircularProgressIndicator(),
                );
              case ConnectionState.none:
                return Text('Input a URL to Start');
              case ConnectionState.active:
                return Text('active');
              case ConnectionState.done:
                return new Column(
                    children: <Widget>[
                      Text('icon:${snapshot.data?.icon}'),
                      Text('title:${snapshot.data?.title}'),
                      Text('url:${snapshot.data?.url}'),
                      Text('statusBarColor:${snapshot.data?.statusBarColor}'),
                      Text('hideAppBar:${snapshot.data?.hideAppBar}')
                    ],
                );
            }
          },
        ),
      ),
    );
  }
}


class CommonModel {
   final String icon;
   final String title;
   final String url;
   final String statusBarColor;
   final bool hideAppBar;

  CommonModel({
    required this.icon, required this.title, required this.url, required this.statusBarColor, required this.hideAppBar
});

  factory CommonModel.fromJson(Map<String,dynamic>json){
    return CommonModel(
        icon:json['icon'],
        title:json['title'],
        url:json['url'],
        statusBarColor:json['statusBarColor'],
        hideAppBar:json['hideAppBar'],
    );
  }
}






