import 'package:flutter/material.dart';
import 'dart:convert' ;

import 'package:http/http.dart' as http;

class NetWorkTest extends StatefulWidget {
  const NetWorkTest({super.key});

  @override
  State<NetWorkTest> createState() => _NetWorkTestState();
}

class _NetWorkTestState extends State<NetWorkTest> {
  String showResult='';

  Future<CommonModel> fetchPost() async{
    var url =
    Uri.https('www.geekailab.com', '/io/flutter_app/json/test_common_model.json', {'q': '{http}'});
    var response = await http.get(url);
    final result=json.decode(response.body);
    return  CommonModel.fromJson(result);

  }

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      home: Scaffold(
        appBar:AppBar(
          title: Text('Http'),
        ),
        body: Column(
          children: <Widget>[
            InkWell(
              onTap: (){
                fetchPost().then((CommonModel value){
                  setState(() {
                    showResult='请求结果：\n hideAppBar:${value.hideAppBar}\n icon:${value.icon}\n  title:${value.title}\n url:${value.url} \n statusBarColor:${value.statusBarColor}';
                  });
                });
              },
              child: Text(
                '点我',
                style: TextStyle(fontSize: 28),
              ),
            ),
            Text(showResult)

          ],
        ),

      ),
    );
  }
}


class CommonModel {
   final String icon;
   final String title;
   final String url;
   final String statusBarColor;
   final bool hideAppBar;

  CommonModel({
    required this.icon, required this.title, required this.url, required this.statusBarColor, required this.hideAppBar
});

  factory CommonModel.fromJson(Map<String,dynamic>json){
    return CommonModel(
        icon:json['icon'],
        title:json['title'],
        url:json['url'],
        statusBarColor:json['statusBarColor'],
        hideAppBar:json['hideAppBar'],
    );
  }
}






