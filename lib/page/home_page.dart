import 'package:flutter/material.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
const APPBAR_SCROLL_OFFSET=100;
class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
 List _imageUrls=[
   'https://www.aiqianqiu.com/attachment/20231108/bfff4fc8ac1e4546a81e0fc500a0ce19.png',
   'https://www.aiqianqiu.com/attachment/20231108/bbdad30fe62f4dcfa64832cbf61204bc.png',
   'https://www.aiqianqiu.com/attachment/20231106/b0a0148a6a604a40973d807eebc2e069.png'
 ];

 double appBarAlpha=0;
 _onScroll(offset) {
   //print('picels:$pixels');
   double alpha=offset/APPBAR_SCROLL_OFFSET;
   if(alpha<0.0){
     appBarAlpha=0;
   }else if(alpha>1.0){
     appBarAlpha=1;
   }else{
     appBarAlpha=alpha;
   }
   setState(() {
   });
   print('appBarAlpha:$appBarAlpha');
   
 }

  @override
  Widget build(BuildContext context) {
    return    Scaffold(
      body:   Stack(
        children: <Widget>[
          MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: NotificationListener(
                onNotification:  (scrollNotification){
                  if(scrollNotification is ScrollUpdateNotification&&scrollNotification.depth==0){
                    _onScroll(scrollNotification.metrics.pixels);
                  }
                  return true;
                },
                child: ListView(
                  children: <Widget>[
                    Container(
                        height: 200,
                        child: Swiper(
                          itemCount: _imageUrls.length,
                          autoplay: true,
                          itemBuilder: (BuildContext context,int index){
                            return new Image.network(
                                _imageUrls[index],
                                fit: BoxFit.fill
                            );
                          },
                          pagination: new SwiperPagination(
                            margin: new EdgeInsets.all(10.0),
                          ),
                        )
                    ),
                    Container(
                      height: 800,
                      child: Text('hello'),
                    )
                  ],
                )
            ),
          ),
          Opacity(
              opacity: appBarAlpha,
             // opacity: 1,
            child: Container(
              height: 80,
              decoration: BoxDecoration(color: Colors.white),
              child: Center(
                child: Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: Text('首页'),
                ),
              ),
            ),
          )

        ],

      )
    );
  }


}
