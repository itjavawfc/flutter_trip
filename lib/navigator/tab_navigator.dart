
import 'package:flutter/material.dart';
import 'package:flutter_trip/page/home_page.dart';

import '../page/mine_page.dart';
import '../page/scene_page.dart';

class TabNavigator extends StatefulWidget {
  const TabNavigator({super.key});

  @override
  State<TabNavigator> createState() => _TabNavigatorState();
}

class _TabNavigatorState extends State<TabNavigator> {

  final _defaultColor=Colors.grey;
  final _activeColor=Colors.blue;
  int _currentIndex=0;

  final PageController  _controller=PageController(
    initialPage: 0,
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          body: PageView(
            controller: _controller,
            children: <Widget>[
              HomePage(),
              Scene(),
              Mine()
            ],
          ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        type: BottomNavigationBarType.fixed,
        onTap: (index){
          _controller.jumpToPage(index);
          setState(() {
             _currentIndex=index;
          });
        },
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home,color: _defaultColor),
              activeIcon: Icon(Icons.home,color:_activeColor),
              label: '首页'
              ),
          BottomNavigationBarItem(
              icon: Icon(Icons.ac_unit_outlined,color: _defaultColor),
              activeIcon: Icon(Icons.ac_unit_outlined,color:_activeColor),
              label: '场景'
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person,color: _defaultColor),
              activeIcon: Icon(Icons.person,color:_activeColor),
              label: '我的'
          ),
        ],
      ),

    );
  }
}
